//
//  hmesh_functions.cpp
//  PyGEL
//
//  Created by Jakob Andreas Bærentzen on 04/11/2017.
//  Copyright © 2017 Jakob Andreas Bærentzen. All rights reserved.
//

#include "hmesh_functions.h"
#include <string>
#include <fstream>
#include <iostream>

#include <GEL/HMesh/triangulate.h>

using namespace std;
using namespace HMesh;


bool valid(const Manifold* m_ptr) {
    return valid(*m_ptr);
}
bool closed(const Manifold* m_ptr) {
    return closed(*m_ptr);
}

void bbox(const Manifold* m_ptr, CGLA::Vec3d* pmin, CGLA::Vec3d* pmax) {
    bbox(*m_ptr, *pmin, *pmax);
}
void bsphere(const Manifold* m_ptr, CGLA::Vec3d* c, double* _r) {
    float r;
    bsphere(*m_ptr, *c, r);
    *_r = r;
}

void stitch_mesh(HMesh::Manifold* m_ptr, double rad) {
    HMesh::stitch_mesh(*m_ptr, rad);
}

bool obj_load(char* fn, HMesh::Manifold* m_ptr) {
    return obj_load(string(fn), *m_ptr, true);
}

bool off_load(char* fn, HMesh::Manifold* m_ptr) {
    return off_load(string(fn), *m_ptr);
}

bool ply_load(char* fn, HMesh::Manifold* m_ptr) {
    return ply_load(string(fn), *m_ptr);
}

bool x3d_load(char* fn, HMesh::Manifold* m_ptr) {
    return x3d_load(string(fn), *m_ptr);
}


bool obj_save(char* fn, HMesh::Manifold* m_ptr) {
    return obj_save(string(fn), *m_ptr);
}

bool off_save(char* fn, HMesh::Manifold* m_ptr) {
    return off_save(string(fn), *m_ptr);

}
bool x3d_save(char* fn, HMesh::Manifold* m_ptr) {
    return x3d_save(string(fn), *m_ptr);
}


void remove_caps(HMesh::Manifold* m_ptr, float thresh) {
    remove_caps(*m_ptr, thresh);
}

void remove_needles(HMesh::Manifold* m_ptr, float thresh, bool averagePositions) {
    remove_needles(*m_ptr, thresh, averagePositions);
}

void close_holes(HMesh::Manifold* m_ptr) {
    close_holes(*m_ptr);
}

void flip_orientation(HMesh::Manifold* m_ptr) {
    flip_orientation(*m_ptr);
}

void minimize_curvature(HMesh::Manifold* m_ptr, bool anneal) {
    minimize_curvature(*m_ptr, anneal);
}

void minimize_dihedral_angle(HMesh::Manifold* m_ptr, int max_iter, bool anneal, bool alpha, double gamma) {
    minimize_dihedral_angle(*m_ptr, max_iter, anneal, alpha, gamma);
}


void maximize_min_angle(HMesh::Manifold* m_ptr, float thresh, bool anneal) {
    maximize_min_angle(*m_ptr, thresh, anneal);
}

void optimize_valency(HMesh::Manifold* m_ptr, bool anneal) {
    optimize_valency(*m_ptr, anneal);
}

void randomize_mesh(HMesh::Manifold* m_ptr, int max_iter) {
    randomize_mesh(*m_ptr, max_iter);
}

void quadric_simplify(HMesh::Manifold* m_ptr, double keep_fraction,
                      double singular_thresh,
                      bool choose_optimal_positions) {
    quadric_simplify(*m_ptr, keep_fraction, singular_thresh, choose_optimal_positions);
}

float average_edge_length(const HMesh::Manifold* m_ptr) {
    return average_edge_length(*m_ptr);
}

float median_edge_length(const HMesh::Manifold* m_ptr) {
    return median_edge_length(*m_ptr);
}

int refine_edges(HMesh::Manifold* m_ptr, float t) {
    return refine_edges(*m_ptr, t);
}

void cc_split(HMesh::Manifold* m_ptr) {
    cc_split(*m_ptr, *m_ptr);
}

void loop_split(HMesh::Manifold* m_ptr) {
    loop_split(*m_ptr, *m_ptr);
}

void root3_subdivide(HMesh::Manifold* m_ptr) {
    root3_subdivide(*m_ptr, *m_ptr);
}

void rootCC_subdivide(HMesh::Manifold* m_ptr) {
    rootCC_subdivide(*m_ptr, *m_ptr);
}

void butterfly_subdivide(HMesh::Manifold* m_ptr) {
    butterfly_subdivide(*m_ptr, *m_ptr);
}

void cc_smooth(HMesh::Manifold* m_ptr) {
    cc_smooth(*m_ptr);
}

void loop_smooth(HMesh::Manifold* m_ptr) {
    loop_smooth(*m_ptr);
}

void shortest_edge_triangulate(HMesh::Manifold* m_ptr) {
    shortest_edge_triangulate(*m_ptr);
}

void extrude_faces(Manifold & m, size_t face_number, int* face_ids) {
	ofstream file;
	file.open("extruding_multiple.log");
	file << "Starting to extrude " << face_number << " faces" << endl;
	FaceSet faceSet;
	for (int i = 0; i < face_number; i++) {
		FaceID f = FaceID(face_ids[i]);
		faceSet.insert(f);
		file << "Face: " << f << " with face_id from unity: " << face_ids[i] << " index " << i << endl;
	}
	file.close();
	extrude_face_set(m, faceSet);
}

void extrude_face_set(Manifold& m, const FaceSet& face_set)
{
	HalfEdgeSet hset;

	for (auto f : face_set)
		circulate_face_ccw(m, f, [&](Walker& w) {
			if (face_set.find(w.opp().face()) == face_set.end()) {
				hset.insert(w.halfedge());
			}
		});

	extrude_along_edge_loop(m, hset);
}

void extrude_along_edge_loop(Manifold& m, const HalfEdgeSet& hset)
{
	ofstream file;
	file.open("extrusion.log");

	vector<pair<HalfEdgeID, HalfEdgeID>> h_pairs;

	file << "extruding " << hset.size() << " edges, vertices of val ";
	for (auto h : hset) {
		auto w = m.walker(h);
		file << valency(m, w.vertex()) << ", ";
		h_pairs.push_back(make_pair(w.halfedge(), w.opp().halfedge()));
	}
	file << endl;

	file << "Vertices before slitting: " << endl;
	for (auto v : m.vertices()) {
		file << "Vertex: " << v << " Position: " << m.pos(v) << endl;
	}

	file << endl << " === SLITTING IN PROGRESS ===" << endl;
	// Slit edges
	for (auto h_in : hset) {
		Walker w_in = m.walker(h_in);
		VertexID v = w_in.vertex();
		for (auto h_out : hset) {
			Walker w_out = m.walker(h_out);
			if (w_out.opp().vertex() == v) {
				file << "Slitting vertex: " << v << " position: " << m.pos(v) << endl;
				VertexID new_v = m.slit_vertex(v, h_in, h_out);
				file << "Resulting slit vertex: " << new_v << " position: " << m.pos(new_v) << endl;
				m.pos(new_v) = m.pos(v);
				file << "Resulting slit vertex after correction: " << new_v << " position: " << m.pos(new_v) << endl;
				break;
			}
		}
	}

	file << "Vertices after slitting: " << endl;
	for (auto v : m.vertices()) {
		file << "Vertex: " << v << " Position: " << m.pos(v) << endl;
	}

	VertexAttributeVector<int> cluster_id(m.allocated_vertices(), -1);
	int CLUSTER_CNT = 0;
	auto assign_cluster_id = [&](VertexID v) {
		if (cluster_id[v] == -1) {
			CLUSTER_CNT++;
			cluster_id[v] = CLUSTER_CNT;
		}
	};

	file << " === INIT ===" << endl;
	for (auto v : m.vertices()) {
		file << "Vertex " << v.get_index() << " Cluster ID : " << cluster_id[v] << " POSITION " << m.pos(v) << endl;
	}

	file << endl << "PROGRESS" << endl;
	FaceSet new_faces;
	// Make new faces
	for (auto h_pair : h_pairs)
	{
		Walker w1 = m.walker(h_pair.first);
		Walker w2 = m.walker(h_pair.second);
		assign_cluster_id(w1.vertex());
		assign_cluster_id(w1.opp().vertex());
		assign_cluster_id(w2.vertex());
		assign_cluster_id(w2.opp().vertex());

		file << "Vertex " << w1.vertex().get_index() << " CLUSTER ASSIGNED: " << cluster_id[w1.vertex()]  << " REFERENCE: " << &cluster_id[w1.vertex()] << " POSITION " << m.pos(w1.vertex()) << endl;
		file << "Vertex " << w1.opp().vertex().get_index() << " CLUSTER ASSIGNED: " << cluster_id[w1.opp().vertex()] << " REFERENCE: " << &cluster_id[w1.opp().vertex()] << " POSITION " << m.pos(w1.opp().vertex()) << endl;
		file << "Vertex " << w2.vertex().get_index() << " CLUSTER ASSIGNED: " << cluster_id[w2.vertex()] << " REFERENCE: " << &cluster_id[w2.vertex()] << " POSITION " << m.pos(w2.vertex()) << endl;
		file << "Vertex " << w2.opp().vertex().get_index() << " CLUSTER ASSIGNED: " << cluster_id[w2.opp().vertex()] << " REFERENCE: " << &cluster_id[w2.opp().vertex()] << " POSITION " << m.pos(w2.opp().vertex()) << endl;
		
		vector<CGLA::Vec3d> pts(4);
		pts[0] = m.pos(w1.vertex());
		pts[1] = m.pos(w1.opp().vertex());
		pts[2] = m.pos(w2.vertex());
		pts[3] = m.pos(w2.opp().vertex());



		FaceID f = m.add_face(pts);
		new_faces.insert(f);
		Walker w = m.walker(f);

		//file << "Vertex " << w.vertex().get_index() << " CLUSTER BEFORE ASSIGN: " << cluster_id[w.vertex()] << " REFERENCE before first assign: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w1.vertex()];
		file << "Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w1.vertex()];
		file << "Fixed Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		w = w.next();
		//file << "Vertex " << w.vertex().get_index() << " CLUSTER BEFORE ASSIGN: " << cluster_id[w.vertex()] << " REFERENCE before first assign: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w1.opp().vertex()];
		file << "Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w1.opp().vertex()];
		file << "Fixed Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		w = w.next();
		//file << "Vertex " << w.vertex().get_index() << " CLUSTER BEFORE ASSIGN: " << cluster_id[w.vertex()] << " REFERENCE before first assign: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w2.vertex()];
		file << "Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w2.vertex()];
		file << "Fixed Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		w = w.next();
		//file << "Vertex " << w.vertex().get_index() << " CLUSTER BEFORE ASSIGN: " << cluster_id[w.vertex()] << " REFERENCE before first assign: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w2.opp().vertex()];
		file << "Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;
		cluster_id[w.vertex()] = cluster_id[w2.opp().vertex()];
		file << "Fixed Vertex " << w.vertex().get_index() << " CLUSTER COPIED: " << cluster_id[w.vertex()] << " REFERENCE: " << &cluster_id[w.vertex()] << " POSITION " << m.pos(w.vertex()) << endl;

	}
	file << " === RES ===" << endl;
	for (auto v : m.vertices()) {
		file << "Vertex " << v.get_index() << " Cluster ID : " << cluster_id[v] << " POSITION " << m.pos(v) << endl;
	}

	// Stitch
	stitch_mesh(m, cluster_id);
	
	file.close();
	//return new_faces;
}


void triangulate(Manifold& manifold) {
	shortest_edge_triangulate(manifold);
	//triangulate_by_vertex_face_split(manifold);
}

void to_idfs(HMesh::Manifold& m, double* points, double* triangles) {
	vector<CGLA::Vec3d> pts;
	VertexAttributeVector<int> vertex_indices;
	ofstream file;
	file.open("to_idfs.log");
	int i = 0;
	for (auto v : m.vertices()) {
		pts.push_back(m.pos(v));
		vertex_indices[v] = i;
		++i;
	}

	vector<CGLA::Vec3i> faces;
	for (auto f : m.faces()) {
		cout << f.get_index() << endl;
		f = FaceID(f.get_index());
		CGLA::Vec3i face;
		int j = 0;
		circulate_face_ccw(m, f, [&](VertexID v) {
			face[j++] = vertex_indices[v];
		});
		faces.push_back(face);
	}

	file << "Points:" << endl;
	i = 0; 
	for (auto& p : pts) {
		file << p << endl;
		points[3*i] = p[0];
		points[3*i + 1] = p[1];
		points[3*i + 2] = p[2];
		i++;
	}
	file << "Triangles:" << endl;
	i = 0;
	for (auto& f : faces) {
		file << f << endl;
		triangles[3 * i] = f[0];
		triangles[3 * i + 1] = f[1];
		triangles[3 * i + 2] = f[2];
		i++;
	}
}

void move_face_along_vector(HMesh::Manifold& m, int face_id, double* direction) {
	ofstream file;
	file.open("movement.log");
	file << "Before movement:" << endl;
	for (auto v : m.vertices()) {
		file << m.pos(v) << endl;
	}
	auto f = FaceID(face_id);
	circulate_face_ccw(m, f, [&](VertexID v) {
		auto old_pos = m.pos(v);
		auto new_pos = CGLA::Vec3d();
		new_pos[0] = old_pos[0] + direction[0];
		new_pos[1] = old_pos[1] + direction[1];
		new_pos[2] = old_pos[2] + direction[2];
		m.pos(v) = new_pos;
	});
	file << "After movement:" << endl;
	for (auto v : m.vertices()) {
		file << m.pos(v) << endl;
	}
	file.close();
}

void move_faces_along_vector(HMesh::Manifold& m, size_t number_of_faces, int* face_ids, double* direction) {
	ofstream file;
	file.open("multiple_movement.log");
	vector<VertexID> passed;
	for (int i = 0; i < number_of_faces; i++) {
		auto f = FaceID(face_ids[i]);
		circulate_face_ccw(m, f, [&](VertexID v) {
			bool is_passed = false;
			for (auto p : passed) {
				if (is_passed = p == v) break;
			}
			if (!is_passed) {
				file << "Moving vertex: " << v << endl;
				passed.push_back(v);
				auto old_pos = m.pos(v);
				auto new_pos = CGLA::Vec3d();
				new_pos[0] = old_pos[0] + direction[0];
				new_pos[1] = old_pos[1] + direction[1];
				new_pos[2] = old_pos[2] + direction[2];
				m.pos(v) = new_pos;
			}
		});
	}
	file.close();
}


void move_vertex_along_vector(HMesh::Manifold& m, int vertex_id, double* direction) {
	auto v = VertexID(vertex_id);
	auto old_pos = m.pos(v);
	auto new_pos = CGLA::Vec3d();
	new_pos[0] = old_pos[0] + direction[0];
	new_pos[1] = old_pos[1] + direction[1];
	new_pos[2] = old_pos[2] + direction[2];
	m.pos(v) = new_pos;
}

void move_faces_along_normal(HMesh::Manifold& m, size_t number_of_faces, int* face_ids, double magnitude) {
	ofstream file;
	file.open("movement_along_normal.log");
	for (int i = 0; i < number_of_faces; i++) {
		auto f = FaceID(face_ids[i]);
		auto n = normal(m, f);
		n[0] *= magnitude;
		n[1] *= magnitude;
		n[2] *= magnitude;

		circulate_face_ccw(m, f, [&](VertexID v) {
			file << "Moving vertex: " << v << endl;
			auto old_pos = m.pos(v);
			auto new_pos = CGLA::Vec3d();
			new_pos[0] = old_pos[0] + n[0];
			new_pos[1] = old_pos[1] + n[1];
			new_pos[2] = old_pos[2] + n[2];
			m.pos(v) = new_pos;
		});
	}
	file.close();
}

void center(Manifold& m_ptr, int _f, double* c) {
	auto face = FaceID(_f);
	int i = 0;
	int n = circulate_face_ccw(m_ptr, face, ([&](VertexID v) {
		auto v_pos = m_ptr.pos(v);
		c[0] += v_pos[0];
		c[1] += v_pos[1];
		c[2] += v_pos[2];
	}));

	c[0] /= n;
	c[1] /= n;
	c[2] /= n;
}

void get_neighbouring_faces_and_edge_centers(HMesh::Manifold& m, int face_id, int* neighbour_faces, double* edge_centers) {
	ofstream file;
	file.open("neighbours.log");
	file << "Finding neighbours for face: " << face_id << endl;
	int i = 0;
	circulate_face_ccw(m, FaceID(face_id), [&](HalfEdgeID h) {
		auto w = m.walker(h);
		auto v1 = m.pos(w.vertex());
		auto v2 = m.pos(w.opp().vertex());
		auto opp_f = w.opp().face();
		file << "Neighbour: " << opp_f << " with index: " << opp_f.get_index() << endl;
		neighbour_faces[i]	= opp_f.get_index();
		edge_centers[3*i]	= (v1[0] + v2[0]) / 2;
		edge_centers[3*i+1] = (v1[1] + v2[1]) / 2;
		edge_centers[3*i+2] = (v1[2] + v2[2]) / 2;
		i++;
	});
	
	
	file << endl;
	file.close();
}

void log_manifold(HMesh::Manifold& m, char* file_name) {
	vector<CGLA::Vec3d> pts;
	VertexAttributeVector<int> vertex_indices;
	ofstream file;
	file.open("manifold.log");
	file << file_name << endl;
	vector<bool> is_used;
	int i = 0;
	for (auto v : m.vertices()) {
		pts.push_back(m.pos(v));
		vertex_indices[v] = i;
		is_used.push_back(m.in_use(v));
		++i;
	}

	vector<CGLA::Vec3i> triangles;
	for (auto f : m.faces()) {
		cout << f.get_index() << endl;
		f = FaceID(f.get_index());
		CGLA::Vec3i face;
		int j = 0;
		circulate_face_ccw(m, f, [&](VertexID v) {
			face[j++] = vertex_indices[v];
		});
		triangles.push_back(face);
	}

	file << "Points:" << endl;
	for (auto& p : pts)
		file << p << endl;
	file << "Is point used:" << endl;
	for (auto& u : is_used)
		file << u << endl;
	file << "Triangles:" << endl;
	for (auto& f : triangles)
		file << f << endl;

	file.close();
}

void log_manifold_original(HMesh::Manifold& m, char* file_name) {
	vector<CGLA::Vec3d> pts;
	VertexAttributeVector<int> vertex_indices;
	ofstream file;
	file.open("manifold_original.log");
	file << file_name << endl;
	vector<bool> is_used;
	int i = 0;
	for (auto v : m.vertices()) {
		pts.push_back(m.pos(v));
		vertex_indices[v] = i;
		is_used.push_back(m.in_use(v));
		++i;
	}

	vector<vector<int>> triangles;
	for (auto f : m.faces()) {
		cout << f.get_index() << endl;
		f = FaceID(f.get_index());
		vector<int> face;
		int j = 0;
		circulate_face_ccw(m, f, [&](VertexID v) {
			face.push_back(vertex_indices[v]);
			//face[j++] = vertex_indices[v];
		});
		triangles.push_back(face);
	}

	file << "Points:" << endl;
	for (auto& p : pts)
		file << p << endl;
	file << "Faces:" << endl;
	for (auto& f : triangles) {
		file << "[ ";
		for (auto& fv : f)
			file << fv << " ";
		file << "]" << endl;
	}

	file.close();
}

void test_valery(HMesh::Manifold& m) {
	vector<CGLA::Vec3d> pts;
	VertexAttributeVector<int> vertex_indices;
	ofstream file;
	file.open("test.log");
	int i = 0;
	for (auto v : m.vertices()) {
		pts.push_back(m.pos(v));
		vertex_indices[v] = i;
		++i;
	}

	vector<CGLA::Vec3i> triangles;
	for (auto f : m.faces()) {
		cout << f.get_index() << endl;
		f = FaceID(f.get_index());
		CGLA::Vec3i face;
		int j = 0;
		circulate_face_ccw(m, f, [&](VertexID v) {
			face[j++] = vertex_indices[v];
		});
		triangles.push_back(face);
	}

	file << "Points:" << endl;
	for (auto& p : pts)
		file << p << endl;
	file << "Triangles:" << endl;
	for (auto& f : triangles)
		file << f << endl;

	file.close();
}

void test_extrude_and_move(HMesh::Manifold& m) {
	int face_id = 0;
	int* faces = new int[1];
	faces[0] = face_id;
	FaceSet faceSet;
	for (auto f : m.faces()) {
		faceSet.insert(f);
		break;
	}
	//faceSet.insert(m.faces()[0])
	//extrude_face_set(m, faceSet);
	extrude_faces(m, 1, faces);
	double* direction = new double[3];
	direction[0] = -0.5;
	direction[1] = 0;
	direction[2] = -0.5;
	move_face_along_vector(m, face_id, direction);
}
