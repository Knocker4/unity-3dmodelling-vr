﻿using System;
using System.Runtime.InteropServices;

namespace Assets.GEL
{
    public abstract class IManifold
    {
        [DllImport("GELExt")]
        protected static extern IntPtr Manifold_new();
        
        [DllImport("GELExt")]
        protected static extern IntPtr Manifold_copy(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern void Manifold_delete(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern int Manifold_positions(IntPtr manifold, double[,] pos);

        [DllImport("GELExt")]
        protected static extern void Manifold_pos(IntPtr manifold, int vertex_id, double[] pos);
        [DllImport("GELExt")]
        protected static extern int Manifold_no_allocated_vertices(IntPtr manifold);
        
        [DllImport("GELExt")]
        protected static extern int Manifold_no_allocated_faces(IntPtr manifold);
        
        [DllImport("GELExt")]
        protected static extern int Manifold_no_allocated_halfedges(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern int Manifold_halfedges(IntPtr manifold, IntPtr[] edges);

        [DllImport("GELExt")]
        // verts -> IntVector
        protected static extern int Manifold_vertices(IntPtr manifold, IntPtr[] verts);
        
        [DllImport("GELExt")]
        // faces -> IntVector
        protected static extern int Manifold_faces(IntPtr manifold, IntPtr[] faces);

        [DllImport("GELExt")]
        protected static extern void Manifold_add_face(IntPtr self, int no_verts, double[] pos);

        [DllImport("GELExt")]
        protected static extern void randomize_mesh(IntPtr manifold, int max_iter);
        
        [DllImport("GELExt")]
        protected static extern void extrude_faces(IntPtr manifold, int face_number, int[] faces);

        [DllImport("GELExt")]
        protected static extern void triangulate(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern void to_idfs(IntPtr manifold, double[] points, double[] triangles);

        [DllImport("GELExt")]
        protected static extern bool Manifold_vertex_in_use(IntPtr manifold, int vertex_id);

        [DllImport("GELExt")]
        protected static extern void move_vertex_along_vector(IntPtr manifold, int vertex_id, double[] direction);
        
        [DllImport("GELExt")]
        protected static extern void move_face_along_vector(IntPtr manifold, int face_id, double[] direction);

        [DllImport("GELExt")]
        protected static extern void move_faces_along_vector(IntPtr manifold, int number_of_faces, int[] faceIds, double[] direction);

        [DllImport("GELExt")]
        protected static extern void move_faces_along_normal(IntPtr manifold, int number_of_faces, int[] faceIds, double magnitude);
        
        [DllImport("GELExt")]
        protected static extern void stitch_mesh(IntPtr manifold, double radius);

        [DllImport("GELExt")]
        protected static extern void Manifold_cleanup(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern void log_manifold(IntPtr manifold, char[] file_name);

        [DllImport("GELExt")]
        protected static extern void center(IntPtr manifold, int face_id, double[] center);

        [DllImport("GELExt")]
        protected static extern void get_neighbouring_faces_and_edge_centers(IntPtr manifold, int face_id, int[] neighbour_faces, double[] edge_centers);
        
        [DllImport("GELExt")]
        protected static extern void face_normal(IntPtr manifold, int face_id, double[] normal);

        [DllImport("GELExt")]
        protected static extern void vertex_normal(IntPtr manifold, int vertex_id, double[] normal);
        
        [DllImport("GELExt")]
        protected static extern void test_extrude_and_move(IntPtr manifold);

        [DllImport("GELExt")]
        protected static extern void log_manifold_original(IntPtr manifold, char[] file_name);
    }
}