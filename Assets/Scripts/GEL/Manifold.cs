﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;

namespace Assets.GEL
{
    public class Manifold : IManifold
    {
        [DllImport("GELExt")]
        public static extern void test_valery(IntPtr manifold);

//        [DllImport("GELExt")]
//        public static extern void extrude_face_set(IntPtr manifold, int[] faces);
        
        private IntPtr _manifold;

        public Manifold()
        {
            _manifold = Manifold_new();
        }

        public Manifold(IntPtr manifold)
        {
            _manifold = manifold;
        }

        ~Manifold()
        {
            Manifold_delete(_manifold);
        }

        public Manifold Copy()
        {
            return new Manifold(Manifold_copy(_manifold));
        }

        public int NumberOfAllocatedVertices()
        {
            return Manifold_no_allocated_vertices(_manifold);
        }
        
        public int NumberOfAllocatedFaces()
        {
            return Manifold_no_allocated_faces(_manifold);
        }
        
        public int NumberOfAllocatedHalfEdges()
        {
            return Manifold_no_allocated_halfedges(_manifold);
        }

        public int GetVertices(IntPtr[] vert)
        {
            return Manifold_vertices(_manifold, vert);
        }
        
        public IntVector GetVertices()
        {
//            var n = NumberOfAllocatedVertices();
            var intVector = new IntVector(0);
            Manifold_vertices(_manifold, intVector.GetVector());
            return intVector;
        }

        public int GetFaces(IntPtr[] faces)
        {
            return Manifold_faces(_manifold, faces);
        }        
        
        public IntVector GetFaces()
        {
            var n = NumberOfAllocatedFaces();
            var intVector = new IntPtr[n];
            var i = Manifold_faces(_manifold, intVector);
            return new IntVector(intVector);
        }

        public IntVector GetHalfEdges()
        {
            var n = NumberOfAllocatedHalfEdges();
            var intVector = new IntVector(n);
            Manifold_halfedges(_manifold, intVector.GetVector());
            return intVector;
        }

        public void AddFace(int noVertices, double[] pos)
        {
            Manifold_add_face(_manifold, noVertices, pos);
        }

        public static Manifold RandomMesh(int noIter)
        {
            var random = new Manifold();
            randomize_mesh(random._manifold, noIter);
            return random;
        }

        public void TestValery()
        {
            test_valery(_manifold);
        }

        public void ExtrudeFaces(int[] faces)
        {
            extrude_faces(_manifold, faces.Length, faces);
        }
        
        public double[,] Positions()
        {
            double[,] positions = new double[NumberOfAllocatedVertices(), 3];
            Manifold_positions(_manifold, positions);
            return positions;
        }

        public Vector3 VertexPosition(int vertexId)
        {
            double[] positions = new double[3];
            Manifold_pos(_manifold, vertexId, positions);
            return new Vector3((float) positions[0], (float) positions[1], (float) positions[2]);
        }

        public bool IsVertexInUse(int vertexId)
        {
            return Manifold_vertex_in_use(_manifold, vertexId);
        }

        public void Triangulate()
        {
            triangulate(_manifold);
        }

        public KeyValuePair<double[], double[]> ToIdfs()
        {
            double[] points = new double[NumberOfAllocatedVertices()*3];
            double[] triangles = new double[NumberOfAllocatedFaces()*3];
            to_idfs(_manifold, points, triangles);
            return new KeyValuePair<double[], double[]>(points, triangles);
        }

        public void MoveVertexAlongVector(int vertexId, Vector3 direction)
        {
            move_vertex_along_vector(_manifold, vertexId, new double[] { direction.x , direction.y, direction.z });
        }
        
        public void MoveFaceAlongVector(int faceId, Vector3 direction)
        {
            MoveFaceAlongVector(faceId, new double[] { direction.x, direction.y, direction.z });
        }
        
        public void MoveFaceAlongVector(int faceId, double[] direction)
        {
            move_face_along_vector(_manifold, faceId, direction);
        }

        public void MoveFacesAlongVector(int[] faceIds, Vector3 direction)
        {
            MoveFacesAlongVector(faceIds, new double[] { direction.x, direction.y, direction.z });
        }
        
        public void MoveFacesAlongVector(int[] faceIds, double[] direction)
        {
            move_faces_along_vector(_manifold, faceIds.Length, faceIds, direction);
        }

        public void MoveFacesAlongNormal(int[] faceIds, double magnitude)
        {
            move_faces_along_normal(_manifold, faceIds.Length, faceIds, magnitude);
        }

        public void StitchMesh(double rad)
        {
            stitch_mesh(_manifold, rad);
        }

        public void CleanUp()
        {
            Manifold_cleanup(_manifold);
        }


        public void LogManifold(String fileName)
        {
            Debug.Log("Writing to : " + fileName);
            log_manifold(_manifold, fileName.ToCharArray());
        }

        public void LogManifoldOriginal(String fileName)
        {
            log_manifold_original(_manifold, fileName.ToCharArray());
        }

        public Vector3 GetCenter(int faceId)
        {
            double[] c = new double[3];
            center(_manifold, faceId, c);
            return new Vector3((float) c[0], (float) c[1], (float) c[2]);
        }

        public Vector3 GetFaceNormal(int faceId)
        {
            double[] n = new double[3];
            face_normal(_manifold, faceId, n);
            return new Vector3((float) n[0], (float) n[1], (float) n[2]);
        }

        public Vector3 GetVertexNormal(int vertexId)
        {
            double[] n = new double[3];
            vertex_normal(_manifold, vertexId, n);
            return new Vector3((float) n[0], (float) n[1], (float) n[2]);
        }
        
        public KeyValuePair<int[], Vector3[]> GetAdjacentFaceIdsAndEdgeCenters(int faceId)
        {
            int[] faces = new int[4];
            double[] edge_centers = new double[4 * 3];
            get_neighbouring_faces_and_edge_centers(_manifold, faceId, faces, edge_centers);
            Vector3[] edgeCenters = new Vector3[4];
            for (int i = 0; i < 4; i++)
            {
                edgeCenters[i] = new Vector3((float) edge_centers[3*i], (float) edge_centers[3*i + 1], (float) edge_centers[3*i + 2]);
            }

            return new KeyValuePair<int[], Vector3[]>(faces, edgeCenters);
        }

        public List<KeyValuePair<int, Vector3>> GetAdjacentFaceEdgeCentrePairs(int faceId)
        {
            int[] faces = new int[4];
            double[] edge_centres = new double[4 * 3];
            get_neighbouring_faces_and_edge_centers(_manifold, faceId, faces, edge_centres);
            var kvlist = new List<KeyValuePair<int, Vector3>>();
            for (int i = 0; i < 4; i++)
            {
                var edgeCentre = new Vector3((float) edge_centres[3*i], (float) edge_centres[3*i + 1], (float) edge_centres[3*i + 2]);
                kvlist.Add(new KeyValuePair<int, Vector3>(faces[i], edgeCentre));
            }

            return kvlist;
        }
        
        public void TestExtrudeAndMove()
        {
            test_extrude_and_move(_manifold);
        }
    }
}
