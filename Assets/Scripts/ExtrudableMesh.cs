﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.GEL;
using Controls;

[RequireComponent(typeof(MeshFilter))]
public class ExtrudableMesh : MonoBehaviour
{
    public ControlsManager Controls;
    public ExtrudableBackstackManager BackstackManager;
    
    private Manifold _manifold;
    public OVRInput.Controller Controller = OVRInput.Controller.LTouch;

    private bool _firstLog = false;
    private float _previousTime = 0;

    // Use this for initialization
    void Start()
    {
        BackstackManager = new ExtrudableBackstackManager();
        _manifold = BuildInitialManifold();
        BackstackManager.SaveState(_manifold);
        _previousTime = Time.time;
//        var faces = new[] {0, 1};
//        StartExtrusion(faces);
//        ExtrudeTo(faces, new Vector3(.3f, .0f, -.3f), DraggableObjectController.MovementMode.FOLLOW);
//        LogTriangulation(_manifold);
//        _manifold.CleanUp();
//        _manifold.ExtrudeFaces(new[] {0});
        //ExtrudeTo(0, new Vector3(0.5f, 0.0f, -0.5f));
//        _manifold.TestExtrudeAndMove();
        //_manifold.StitchMesh(1e-9);
        Controls.SetUp();
        _manifold.LogManifoldOriginal("");
        LogTriangulation(_manifold);
        TriangulateAndDrawManifold(_manifold);
        Controls.UpdateControls(_manifold);
        /* */
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - _previousTime > 1 && OVRInput.Get(OVRInput.Button.Three))
        {
            _manifold = BackstackManager.RestoreState();
            Controls.Clear();
            Controls.UpdateControls(_manifold);
            _previousTime = Time.time;
        }
        TriangulateAndDrawManifold(_manifold);
    }

    public void StartExtrusion(int faceId)
    {
        StartExtrusion(new[] {faceId});
    }

    public void StartExtrusion(int[] faceIds)
    {
        BackstackManager.SaveState(_manifold);
        _manifold.ExtrudeFaces(faceIds);
//        Controls.UpdateLatchesForFaces(_manifold, faceIds.ToList());
    }

    public void ExtrudeTo(int faceId, Vector3 destination)
    {
        var center = _manifold.GetCenter(faceId);
        destination.x /= transform.localScale.x;
        destination.y /= transform.localScale.y;
        destination.z /= transform.localScale.z;
        _manifold.MoveFaceAlongVector(faceId, destination - center);
        Controls.UpdateControls(_manifold);
    }

    public void ExtrudeTo(
        int[] faceIds, 
        Vector3 destination, 
        FaceHandleController.MovementMode mode = FaceHandleController.MovementMode.FOLLOW)
    {
        switch (mode)
        {
            case FaceHandleController.MovementMode.FOLLOW:
            {
                int faceId = faceIds[0];
                var center = _manifold.GetCenter(faceId);
                destination.x /= transform.localScale.x;
                destination.y /= transform.localScale.y;
                destination.z /= transform.localScale.z;
                _manifold.MoveFacesAlongVector(faceIds, destination - center);
                Controls.UpdateControls(_manifold);       
                break;
            }
            case FaceHandleController.MovementMode.SCALE:
            {
                int faceId = faceIds[0];
                var center = _manifold.GetCenter(faceId);
                var normal = _manifold.GetFaceNormal(faceId);
                destination.x /= transform.localScale.x;
                destination.y /= transform.localScale.y;
                destination.z /= transform.localScale.z;
                var project = Vector3.Project(destination - center, normal);
                _manifold.MoveFacesAlongNormal(faceIds, project.magnitude * Math.Sign(Vector3.Dot(project, normal)));
                Controls.UpdateControls(_manifold);
                break;
            }
        }
    }

    public void MoveVertexTo(int vertexId, Vector3 destination)
    {
        var position = _manifold.VertexPosition(vertexId);
        destination.x /= transform.localScale.x;
        destination.y /= transform.localScale.y;
        destination.z /= transform.localScale.z;
        _manifold.MoveVertexAlongVector(vertexId, destination - position);
        Controls.UpdateControls(_manifold);
    }

    /* UTIL FUNCTIONS */

    public FaceHandleController GetHandle(int faceId)
    {
        return 
            Controls.GetHandle(faceId);
    }
    
    private static Manifold BuildInitialManifold()
    {
        var manifold = new Manifold();

        double[] bottom =
        {
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, 1.0, -1.0,
            1.0, 1.0, -1.0
        };

        double[] front =
        {
            1.0, -1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0
        };

        double[] left =
        {
            1.0, -1.0, -1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,
            -1.0, -1.0, -1.0
        };

        double[] right =
        {
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
        };

        double[] top =
        {
            1.0, -1.0, 1.0,
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0
        };

        double[] back =
        {
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
        };

        manifold.AddFace(4, bottom);
        manifold.AddFace(4, front);
        manifold.AddFace(4, left);
        manifold.AddFace(4, right);
        manifold.AddFace(4, top);
        manifold.AddFace(4, back);

        manifold.StitchMesh(1e-10);

        return manifold;
    }

    private static void LogTriangulation(Manifold manifold)
    {
        Manifold triangulated = manifold.Copy();
        triangulated.Triangulate();
        triangulated.LogManifold("");
    }


    void TriangulateAndDrawManifold(Manifold manifold)
    {
        Manifold triangulated;
        triangulated = manifold.Copy();
        triangulated.Triangulate();

        var pointsAndTriangles = triangulated.ToIdfs();

        var points = pointsAndTriangles.Key;
        var vertices = new Vector3[triangulated.NumberOfAllocatedVertices()];
        var uvs = new Vector2[triangulated.NumberOfAllocatedVertices()];
        for (var i = 0; i < triangulated.NumberOfAllocatedVertices(); i++)
        {
            vertices[i] = new Vector3((float) points[3 * i], (float) points[3 * i + 1], (float) points[3 * i + 2]);
            uvs[i] = new Vector2((float) points[3 * i + 1], (float) points[3 * i + 2]);
        }

        var triangles = pointsAndTriangles.Value;
        var triang = new int[triangulated.NumberOfAllocatedFaces() * 3];
        for (var i = 0; i < triangulated.NumberOfAllocatedFaces(); i++)
        {
            triang[3 * i] = (int) triangles[3 * i];
            triang[3 * i + 1] = (int) triangles[3 * i + 1];
            triang[3 * i + 2] = (int) triangles[3 * i + 2];
        }

        if (_firstLog)
        {
            using (StreamWriter file =
                new StreamWriter(@"D:\WORKSPACE\Unity\TryOpenVR\Mesh.log"))
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    file.WriteLine("Point: " + vertices[i].x + " "
                                   + vertices[i].y + " "
                                   + vertices[i].z);
                }

                for (int i = 0; i < triangulated.NumberOfAllocatedFaces(); i++)
                {
                    file.WriteLine("Triangle: " + triang[3 * i] + " " + triang[3 * i + 1] + " " + triang[3 * i + 2]);
                }
            }
        }

        var mesh = GetComponent<MeshFilter>().mesh;
        mesh.name = "extruded";
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triang;
        mesh.RecalculateNormals();
    }
}