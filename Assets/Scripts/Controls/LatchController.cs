﻿using UnityEngine;
using UnityEngine.XR.WSA.WebCam;

namespace Controls
{
    public class LatchController : IInteractableObject
    {
        public int FirstFace = -1;
        public int SecondFace = -1;

        public bool Locked;

        private Material _latchMaterial;

        public Vector3 LookAtVector;

        private void Start()
        {
//            Locked = false;
            _latchMaterial = GetComponent<Renderer>().material;
            UpdateMaterialColor();
        }

        private void Update()
        {
            UpdateMaterialColor();
        }

        public void UpdateMaterialColor()
        {
            if (_latchMaterial == null) return;
            _latchMaterial.color = Locked ? Color.red : Color.white;
        }

        public bool IsAdjacent(int faceId1, int faceId2)
        {
            return FirstFace == faceId1 && SecondFace == faceId2 ||
                   FirstFace == faceId2 && SecondFace == faceId1;
        }

        public int GetOtherFace(int faceId)
        {
            return FirstFace == faceId ? SecondFace : FirstFace;
        }

        public override void Interact()
        {
            Locked = !Locked;
            if (LookAtVector != null)
                transform.rotation = Quaternion.LookRotation((Locked ? 1 : -1) * LookAtVector);
            UpdateMaterialColor();
        }

        public override void ChangeInteraction(InteractionMode mode = InteractionMode.SINGLE) {}

        public override void StopInteraction() {}
    }
}