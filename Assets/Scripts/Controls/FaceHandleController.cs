﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Controls
{
    [RequireComponent(typeof(MeshCollider))]
    public class FaceHandleController : IInteractableObject
    {
        public enum MovementMode
        {
            FOLLOW,
            SCALE
        }
        
        public ExtrudableMesh Extrudable;
        public int AssociatedFaceID;

        private Vector3 screenPoint;
        private Vector3 offset;
        private FixedJoint _joint;

        private int _framesToSkip = FRAMES_TO_SKIP;

        public bool IsMouseEnabled = false;
        public bool IsDragged = false;

        private const int FRAMES_TO_SKIP = 1;
        
        public List<GameObject> ConnectedLatches = new List<GameObject>();

        [SerializeField]
        private List<int> extrudingFaces = new List<int>();

        [SerializeField]
        private MovementMode _movementMode = MovementMode.FOLLOW;
        
        private void Update()
        {
            if (--_framesToSkip != 0) return;
            if (_joint != null)
                Extrudable.ExtrudeTo(
                    extrudingFaces.ToArray(),
                    transform.position,
                    _movementMode);
            _framesToSkip = FRAMES_TO_SKIP;
        }

        public void AttachLatch(GameObject latch)
        {
            if (!ConnectedLatches.Contains(latch))
                ConnectedLatches.Add(latch);
        }

        public void ClearLatches()
        {
            ConnectedLatches.Clear();
        }

        public void AttachLatches(List<GameObject> latches)
        {
            ConnectedLatches.AddRange(latches);
        }

        void OnMouseUp()
        {
            if (IsMouseEnabled)
            {
                IsDragged = false;
                Extrudable.ExtrudeTo(AssociatedFaceID, transform.position);
            }
        }

        void OnMouseDown()
        {
            if (IsMouseEnabled) // Only do if IsDraggable == true
            {
                IsDragged = true;
                screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

                offset = gameObject.transform.position -
                         Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                             screenPoint.z));

                Extrudable.StartExtrusion(AssociatedFaceID);
            }
        }

        void OnMouseDrag()
        {
            if (IsMouseEnabled) // Only do if IsDraggable == true
            {
                Vector3 curScreenPoint =
                    new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                        screenPoint.z); // hardcode the y and z for your use

                Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
                transform.position = curPosition;
                Extrudable.ExtrudeTo(AssociatedFaceID, transform.position);
            }
        }

        private List<KeyValuePair<int, bool>> GetAdjacentLockedFaces(int faceId, List<KeyValuePair<int, bool>> currentFaceSet)
        {
            var handle = Extrudable.GetHandle(faceId);
            if (handle == null)
                return currentFaceSet.Distinct().ToList();
            currentFaceSet.Add(new KeyValuePair<int, bool>(faceId, handle.IsDragged));
//            var latches = handle.ConnectedLatches.ConvertAll(latch => latch.GetComponent<LatchController>());
            foreach (var latch in handle.ConnectedLatches)
            {
                if (latch != null)
                {
                    var latchController = latch.GetComponent<LatchController>();
                    if (latchController.Locked && !currentFaceSet.ConvertAll(pair => pair.Key).Contains(latchController.GetOtherFace(faceId)))
                    {
                        GetAdjacentLockedFaces(latchController.GetOtherFace(faceId), currentFaceSet);
                    }    
                }
            }
            return currentFaceSet.Distinct().ToList();
        }

//        private bool 
        
        public override void ChangeInteraction(InteractionMode mode = InteractionMode.SINGLE)
        {
            switch (mode)
            {
                case InteractionMode.SINGLE:
                {
                    _movementMode = MovementMode.FOLLOW;
                    Debug.Log("Interaction mode for handle: " + AssociatedFaceID + " Changed to FOLLOW");
                    break;
                }
                case InteractionMode.DUAL:
                {
                    _movementMode = MovementMode.SCALE;
                    Debug.Log("Interaction mode for handle: " + AssociatedFaceID + " Changed to SCALE");
                    break;
                }
            }
        }

        public override void Interact()
        {
            var faceList = GetAdjacentLockedFaces(AssociatedFaceID, new List<KeyValuePair<int, bool>>());
            if (faceList.ConvertAll(pair => pair.Value).Contains(true))
            {
                Debug.Log("Changing the interaction mode");
                Extrudable.GetHandle(faceList.Find(pair => pair.Value).Key).ChangeInteraction(InteractionMode.DUAL);
                ChangeInteraction(InteractionMode.DUAL);
                extrudingFaces = faceList.ConvertAll(pair => pair.Key);
                IsDragged = true;
                _joint = gameObject.AddComponent<FixedJoint>();
                _controllerObject.GetComponent<OvrAvatarHand>().AttachBody(_joint);
            }
            else
            {
                extrudingFaces = faceList.ConvertAll(pair => pair.Key);
                IsDragged = true;
                _joint = gameObject.AddComponent<FixedJoint>();
                _controllerObject.GetComponent<OvrAvatarHand>().AttachBody(_joint);
                Extrudable.Controls.RemoveLatches(extrudingFaces);
//                ConnectedLatches.Clear();
                Extrudable.StartExtrusion(extrudingFaces.ToArray());   
            }
        }

        public override void StopInteraction()
        {
            if (_joint != null)
            {
                IsDragged = false;
                Extrudable.ExtrudeTo(
                    extrudingFaces.ToArray(),
                    transform.position,
                    _movementMode);
                extrudingFaces.Clear();
                if (_movementMode == MovementMode.SCALE)
                {
                    var faceList = GetAdjacentLockedFaces(AssociatedFaceID, new List<KeyValuePair<int, bool>>());
                    Extrudable.GetHandle(faceList.Find(pair => pair.Value).Key).ChangeInteraction();
                    ChangeInteraction();
                }
            }
        }
    }
}