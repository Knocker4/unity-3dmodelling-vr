﻿using System;
using UnityEngine;

namespace Controls
{
    public abstract class IInteractableObject : MonoBehaviour
    {
        public enum InteractionMode
        {
            SINGLE,
            DUAL
        }

        protected GameObject _controllerObject;

        public void StartInteraction(GameObject controllerObject)
        {
            Debug.Log("Got signal for interaction");
            _controllerObject = controllerObject;
            Interact();
        }

        public abstract void Interact();

        public abstract void ChangeInteraction(InteractionMode mode = InteractionMode.SINGLE);

        public abstract void StopInteraction();
    }
}