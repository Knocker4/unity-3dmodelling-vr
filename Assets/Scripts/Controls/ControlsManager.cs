﻿using System.Collections.Generic;
using System.Linq;
using Assets.GEL;
using Collada141;
using UnityEngine;

namespace Controls
{
    public class ControlsManager : MonoBehaviour
    {
        public GameObject DragHandlePrefab;
        public GameObject LatchPrefab;
        public GameObject VertexHandlePrefab;
        public GameObject Controls;
        public ExtrudableMesh Extrudable;

        [SerializeField] private List<GameObject> _handles = new List<GameObject>();
        [SerializeField] private List<GameObject> _latches = new List<GameObject>();
        [SerializeField] private List<GameObject> _vertexHandles = new List<GameObject>();

        private List<KeyValuePair<int, int>> _closedLatchesBackup = new List<KeyValuePair<int, int>>();

        public void SetUp()
        {
            _handles = new List<GameObject>();
            _latches = new List<GameObject>();
            _vertexHandles = new List<GameObject>();
            _closedLatchesBackup = new List<KeyValuePair<int, int>>();
        }

        public void Clear()
        {
            _handles.ForEach(Destroy);
            _handles.Clear();
            _latches.ForEach(Destroy);
            _latches.Clear();
            _vertexHandles.ForEach(Destroy);
            _vertexHandles.Clear();
        }

        public FaceHandleController GetHandle(int faceId)
        {
            FaceHandleController reqHandle = null;
            foreach (var handle in _handles)
            {
                if (handle.GetComponent<FaceHandleController>().AssociatedFaceID == faceId)
                {
                    reqHandle = handle.GetComponent<FaceHandleController>();
                    break;
                }
            }

            return reqHandle;
        }

        public void RemoveLatches(List<int> faces)
        {
            foreach (var face in faces)
            {
                var handle = GetHandle(face);
                if (handle == null)
                    continue;
                
                var handleController = handle.GetComponent<FaceHandleController>();
                
                List<GameObject> latchesToRemove = new List<GameObject>();
                foreach (var latch in handleController.ConnectedLatches)
                {
                    if (latch != null && !latch.GetComponent<LatchController>().Locked)
                    {
                        latchesToRemove.Add(latch);
                    }
                }

                foreach (var latch in latchesToRemove)
                {
                    _latches.Remove(latch);
                    handleController.ConnectedLatches.Remove(latch);
                    GetHandle(latch.GetComponent<LatchController>().GetOtherFace(handleController.AssociatedFaceID)).ConnectedLatches.Remove(latch);
//                    GetHandle(latch.GetComponent<LatchController>().SecondFace).ConnectedLatches.Remove(latch);

                    Destroy(latch);
                }
            }
        }
        
        public void RemoveLatches(List<GameObject> latchesToDestroy)
        {
            latchesToDestroy.ForEach(latch =>
            {
                var latchController = latch.GetComponent<LatchController>();
                if (latchController.Locked)
                {
                    _closedLatchesBackup.Add(new KeyValuePair<int, int>(latchController.FirstFace,
                        latchController.SecondFace));
                }

                var latchToRemove = _latches.Find(otherLatch =>
                {
                    var otherLatchController = otherLatch.GetComponent<LatchController>();
                    return otherLatchController.IsAdjacent(latchController.FirstFace, latchController.SecondFace);
                });
                _latches.Remove(latchToRemove);
                Destroy(latchToRemove);
            });
        }

        public void UpdateLatchesForFaces(Manifold manifold, List<int> faces)
        {
            faces.ForEach(face =>
            {
                _latches.ConvertAll(latch => latch.GetComponent<LatchController>())
                    .FindAll(latch => latch.FirstFace == face || latch.SecondFace == face)
                    .ForEach(latch =>
                    {
                        var adjacentFaces = manifold.GetAdjacentFaceEdgeCentrePairs(face);
                        adjacentFaces.ConvertAll(pair => pair.Key);
                    });
            });
        }

        public void UpdateControls(Manifold manifold)
        {
            var facesVisited = new bool[manifold.NumberOfAllocatedFaces()];
//            foreach (var latch in _latches)
//            {
//                if (!latch.GetComponent<LatchController>().Locked)
//                {
//                    Destroy(latch);   
//                }                    
//            }
            foreach (var handle in _handles)
            {
                var handleController = handle.GetComponent<FaceHandleController>();
                var handleFace = handleController.AssociatedFaceID;

//                    Debug.Log("Started working on updating handles: " + handleController.AssociatedFaceID);
                if (!handleController.IsDragged)
                {
//                        Debug.Log("Found a handle that is not dragged");
//                    var faceId = handleController.AssociatedFaceID;
                    var center = manifold.GetCenter(handleFace);
                    center.x *= Extrudable.gameObject.transform.localScale.x;
                    center.y *= Extrudable.gameObject.transform.localScale.y;
                    center.z *= Extrudable.gameObject.transform.localScale.z;
                    handle.transform.position = Extrudable.gameObject.transform.position + center;

                    // Rotate the handle to look in the opposite direction of face's normal
                    var normal = manifold.GetFaceNormal(handleFace);
                    Quaternion rotation = Quaternion.LookRotation(-normal);
                    handle.transform.rotation = rotation;
                    // Adjust rotation
                    handle.transform.Rotate(0, -90, 0);
//                        Debug.Log("Adjusted it");
                }

                var adjacentFaces = manifold.GetAdjacentFaceEdgeCentrePairs(handleFace);

                foreach (var latch in _latches)
                {
                    var isConnected = false;
                    var latchController = latch.GetComponent<LatchController>();
                    KeyValuePair<int, Vector3> connectedFacePair = new KeyValuePair<int, Vector3>();
                    foreach (var pair in adjacentFaces)
                    {
                        if (latchController.IsAdjacent(handleFace, pair.Key))
                        {
                            isConnected = true;
                            connectedFacePair = pair;
                            break;
                        }
                    }

                    if (!isConnected)
                        continue;
//                            Debug.Log("Going through latch: " + latch.GetComponent<LatchController>().FirstFace + "-" +
//                                      latch.GetComponent<LatchController>().SecondFace);
                    var otherFace = latch.GetComponent<LatchController>()
                        .GetOtherFace(handleFace);

                    var edgeCenter = connectedFacePair.Value;
                    edgeCenter.x *= Extrudable.gameObject.transform.localScale.x;
                    edgeCenter.y *= Extrudable.gameObject.transform.localScale.y;
                    edgeCenter.z *= Extrudable.gameObject.transform.localScale.z;

                    latch.transform.position =
                        Extrudable.gameObject.transform.position + edgeCenter; // + center) / 2;

                    var normal = manifold.GetFaceNormal(handleFace);
                    var adjacentFaceNormal = manifold.GetFaceNormal(otherFace);
                    Quaternion latchRotation = Quaternion.LookRotation((latchController.Locked ? 1 : -1) * (adjacentFaceNormal + normal));
                    latchController.LookAtVector = adjacentFaceNormal + normal;
                    latch.transform.rotation = latchRotation;
                    Debug.Log("Adjusted latch");
                }
            }

            var handleControllers = _handles.ConvertAll(handle => handle.GetComponent<FaceHandleController>());
            for (int i = 0; i < manifold.NumberOfAllocatedFaces(); i++)
            {
                var visitedHandleController =
                    handleControllers.Find(handleController => handleController.AssociatedFaceID == i);

                if (visitedHandleController != null && visitedHandleController.ConnectedLatches.Count >= 4)
                {
                    facesVisited[i] = true;
                    continue;
                }

                Debug.Log("Creating new handle and latches");

                var newHandle = visitedHandleController != null
                    ? visitedHandleController.gameObject
                    : InstantiateHandle(manifold, i);
                InstantiateLatches(manifold, newHandle.GetComponent<FaceHandleController>(), facesVisited);
                facesVisited[i] = true;
            }

            foreach (var vertexHandle in _vertexHandles)
            {
                var vertexHandleController = vertexHandle.GetComponent<VertexController>();
                if (!vertexHandleController.IsDragged)
                {
                    Vector3 vertexPosition = manifold.VertexPosition(vertexHandleController.AssociatedVertexID);
                    vertexPosition.x *= Extrudable.gameObject.transform.localScale.x;
                    vertexPosition.y *= Extrudable.gameObject.transform.localScale.y;
                    vertexPosition.z *= Extrudable.gameObject.transform.localScale.z;    
                    
                    vertexHandle.transform.position = Extrudable.gameObject.transform.position + vertexPosition;
                    
                    Vector3 vertexNormal = manifold.GetVertexNormal(vertexHandleController.AssociatedVertexID);

                    vertexHandle.transform.position = Extrudable.gameObject.transform.position + vertexPosition;
                    
                    Quaternion handleRotation = Quaternion.LookRotation(-vertexNormal);
                    vertexHandle.transform.rotation = handleRotation;
                }    
            }
            
            for (int i = 0; i < manifold.NumberOfAllocatedVertices(); i++)
            {
                var vertexHandle = _vertexHandles.Find(handle =>
                    handle.GetComponent<VertexController>().AssociatedVertexID == i);
                if (!manifold.IsVertexInUse(i) || vertexHandle != null)
                    continue;

                InstantiateVertexHandle(manifold, i);
            }
        }

        private GameObject InstantiateHandle(Manifold manifold, int faceId)
        {
            GameObject newHandle;
            newHandle = Instantiate(DragHandlePrefab);
            newHandle.transform.SetParent(Controls.transform);
            // Scale handle to same size as the meshes scale
            newHandle.transform.localScale = new Vector3(
                newHandle.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                newHandle.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                newHandle.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
            );

            // Assign handle position to the center of a face
            var center = manifold.GetCenter(faceId);
            center.x *= Extrudable.gameObject.transform.localScale.x;
            center.y *= Extrudable.gameObject.transform.localScale.y;
            center.z *= Extrudable.gameObject.transform.localScale.z;
            newHandle.transform.position = Extrudable.gameObject.transform.position + center;

            // Rotate the handle to look in the opposite direction of face's normal
            var normal = manifold.GetFaceNormal(faceId);
            Quaternion rotation = Quaternion.LookRotation(-normal);
            newHandle.transform.rotation = rotation;
            // Adjust rotation
            newHandle.transform.Rotate(0, -90, 0);

            // Assign FaceID and Extrudable reference to handle
            var handleController = newHandle.GetComponent<FaceHandleController>();
            handleController.AssociatedFaceID = faceId;
            handleController.Extrudable = Extrudable;

            _handles.Add(newHandle);

            return newHandle;
        }

        public void InstantiateLatches(Manifold manifold, FaceHandleController handleController,
            bool[] facesVisited)
        {
            var neighbourFaces = manifold.GetAdjacentFaceIdsAndEdgeCenters(handleController.AssociatedFaceID);
            var edgeCenters = neighbourFaces.Value;

            for (int j = 0; j < neighbourFaces.Key.Length; j++)
            {
                var adjacentFace = neighbourFaces.Key[j];

                var latchExists = false;
                GameObject existingLatch = null;
                foreach (var latch in _latches)
                {
                    if (latch.GetComponent<LatchController>()
                        .IsAdjacent(handleController.AssociatedFaceID, adjacentFace))
                    {
                        latchExists = true;
                        existingLatch = latch;
                        break;
                    }
                }
                
                if (latchExists/*facesVisited[adjacentFace]*/)
                {
                    handleController.AttachLatch(existingLatch);
//                    handleController.AttachLatch(
//                        _latches.Find(latch =>
//                            latch.GetComponent<LatchController>()
//                                .IsAdjacent(handleController.AssociatedFaceID, adjacentFace))
//                    );
                }
                else
                {
                    var newLatch = Instantiate(LatchPrefab);
                    newLatch.transform.SetParent(Controls.transform);

                    newLatch.transform.localScale = new Vector3(
                        newLatch.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                        newLatch.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                        newLatch.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
                    );

                    var edgeCenter = edgeCenters[j];
                    edgeCenter.x *= Extrudable.gameObject.transform.localScale.x;
                    edgeCenter.y *= Extrudable.gameObject.transform.localScale.y;
                    edgeCenter.z *= Extrudable.gameObject.transform.localScale.z;

                    newLatch.transform.position =
                        Extrudable.gameObject.transform.position + edgeCenter;

                    var adjacentFaceNormal = manifold.GetFaceNormal(adjacentFace);
                    var normal = manifold.GetFaceNormal(handleController.AssociatedFaceID);
                    Quaternion latchRotation = Quaternion.LookRotation(-(adjacentFaceNormal + normal));
                    newLatch.transform.rotation = latchRotation;

                    var latchController = newLatch.GetComponent<LatchController>();
                    latchController.FirstFace = handleController.AssociatedFaceID;
                    latchController.SecondFace = adjacentFace;
                    latchController.LookAtVector = adjacentFaceNormal + normal;
                    latchController.UpdateMaterialColor();

                    KeyValuePair<int, int> pairToRemove = new KeyValuePair<int, int>();
                    foreach (var pair in _closedLatchesBackup)
                    {
                        if (latchController.IsAdjacent(pair.Key, pair.Value))
                        {
                            pairToRemove = pair;
                            latchController.Locked = true;
                            break;
                        }
                    }

                    _closedLatchesBackup.Remove(pairToRemove);

                    handleController.AttachLatch(newLatch);
                    _latches.Add(newLatch);
                }
            }
        }

        private GameObject InstantiateVertexHandle(Manifold manifold, int vertexId)
        {
            var newHandle = Instantiate(VertexHandlePrefab);
            newHandle.transform.SetParent(Controls.transform);

            Vector3 vertexPosition = manifold.VertexPosition(vertexId);
            vertexPosition.x *= Extrudable.gameObject.transform.localScale.x;
            vertexPosition.y *= Extrudable.gameObject.transform.localScale.y;
            vertexPosition.z *= Extrudable.gameObject.transform.localScale.z;

            Vector3 vertexNormal = manifold.GetVertexNormal(vertexId);

            newHandle.transform.localScale = new Vector3(
                newHandle.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                newHandle.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                newHandle.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
            );

            newHandle.transform.position = Extrudable.gameObject.transform.position + vertexPosition;
            Quaternion handleRotation = Quaternion.LookRotation(-vertexNormal);
            newHandle.transform.rotation = handleRotation;

            var handleController = newHandle.GetComponent<VertexController>();
            handleController.AssociatedVertexID = vertexId;
            handleController.Extrudable = Extrudable;

            _vertexHandles.Add(newHandle);

            return newHandle;
        }

        private void UpdateVertexHandles(Manifold manifold)
        {
            var draggedVertex = -1;
            GameObject draggedHandle = null;
            _vertexHandles.ForEach(handle =>
            {
                if (handle.GetComponent<VertexController>().IsDragged)
                {
                    draggedVertex = handle.GetComponent<VertexController>().AssociatedVertexID;
                    draggedHandle = handle;
                }
                else
                {
                    Destroy(handle);
                }
            });
            _vertexHandles.Clear();
            for (int i = 0; i < manifold.NumberOfAllocatedVertices(); i++)
            {
                if (manifold.IsVertexInUse(i))
                {
                    if (i == draggedVertex && draggedHandle != null)
                    {
                        _vertexHandles.Add(draggedHandle);
                        continue;
                    }

                    var newHandle = Instantiate(VertexHandlePrefab);
                    newHandle.transform.SetParent(Controls.transform);

                    Vector3 vertexPosition = manifold.VertexPosition(i);
                    vertexPosition.x *= Extrudable.gameObject.transform.localScale.x;
                    vertexPosition.y *= Extrudable.gameObject.transform.localScale.y;
                    vertexPosition.z *= Extrudable.gameObject.transform.localScale.z;

                    Vector3 vertexNormal = manifold.GetVertexNormal(i);

                    newHandle.transform.localScale = new Vector3(
                        newHandle.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                        newHandle.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                        newHandle.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
                    );

                    newHandle.transform.position = Extrudable.gameObject.transform.position + vertexPosition;
                    Quaternion handleRotation = Quaternion.LookRotation(-vertexNormal);
                    newHandle.transform.rotation = handleRotation;

                    var handleController = newHandle.GetComponent<VertexController>();
                    handleController.AssociatedVertexID = i;
                    handleController.Extrudable = Extrudable;

                    _vertexHandles.Add(newHandle);
                }
            }
        }


        public void UpdateControls2(Manifold manifold)
        {
            var draggedFace = -1;
            GameObject draggedHandle = null;
            _handles.ForEach(handle =>
            {
                if (!handle.GetComponent<FaceHandleController>().IsDragged)
                {
                    Destroy(handle);
                }
                else
                {
                    draggedFace = handle.GetComponent<FaceHandleController>().AssociatedFaceID;
                    draggedHandle = handle;
                }
            });
            var closedLatches = new List<KeyValuePair<int, int>>();
//            Debug.Log("===== LOOKING FOR CLOSED LATCHES AMONG: " + _latches.Count + " OF THEM =====");
            _latches.ForEach(latch =>
            {
                var controller = latch.GetComponent<LatchController>();
                if (controller.Locked)
                {
//                    Debug.Log("LATCH " + controller.FirstFace + "-" + controller.SecondFace + " is locked, adding it");
                    closedLatches.Add(new KeyValuePair<int, int>(controller.FirstFace, controller.SecondFace));
                }

                Destroy(latch);
            });
//            Debug.Log("========================================");
            _handles.Clear();
            _latches.Clear();
//            Debug.Log("===== CLOSED LATCHES =====");
//            foreach (var closedLatch in closedLatches)
//            {
//                Debug.Log("Latch: " + closedLatch.Key + "-" + closedLatch.Value);
//            }

//            Debug.Log("==========================");

            int[] facesVisited = new int[manifold.NumberOfAllocatedFaces()];

            for (int i = 0; i < manifold.NumberOfAllocatedFaces(); i++)
            {
                GameObject currentHandle;
                facesVisited[i] = 0;
                if (i == draggedFace && draggedHandle != null)
                {
                    currentHandle = draggedHandle;
                    _handles.Add(draggedHandle);
                }
                else
                {
                    // Make the handle
                    currentHandle = Instantiate(DragHandlePrefab);
                    currentHandle.transform.SetParent(Controls.transform);
                    // Scale handle to same size as the meshes scale
                    currentHandle.transform.localScale = new Vector3(
                        currentHandle.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                        currentHandle.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                        currentHandle.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
                    );

                    // Assign handle position to the center of a face
                    var center = manifold.GetCenter(i);
                    center.x *= Extrudable.gameObject.transform.localScale.x;
                    center.y *= Extrudable.gameObject.transform.localScale.y;
                    center.z *= Extrudable.gameObject.transform.localScale.z;
                    currentHandle.transform.position = Extrudable.gameObject.transform.position + center;

                    // Rotate the handle to look in the opposite direction of face's normal
                    var normal = manifold.GetFaceNormal(i);
                    Quaternion rotation = Quaternion.LookRotation(-normal);
                    currentHandle.transform.rotation = rotation;
                    // Adjust rotation
                    currentHandle.transform.Rotate(0, -90, 0);

                    // Assign FaceID and Extrudable reference to handle
                    var handleController = currentHandle.GetComponent<FaceHandleController>();
                    handleController.AssociatedFaceID = i;
                    handleController.Extrudable = Extrudable;

                    _handles.Add(currentHandle);
                }

                // Create latches
                var neighbourFaces = manifold.GetAdjacentFaceIdsAndEdgeCenters(i);
                var edgeCenters = neighbourFaces.Value;

                for (int j = 0; j < neighbourFaces.Key.Length; j++)
                {
                    var adjacentFace = neighbourFaces.Key[j];

                    if (facesVisited[adjacentFace] == 1)
                    {
                        currentHandle.GetComponent<FaceHandleController>().AttachLatch(
                            _latches.Find(latch => latch.GetComponent<LatchController>().IsAdjacent(i, adjacentFace))
                        );
                    }
                    else
                    {
//                    Debug.Log("Processing adjacent face: " + adjacentFace);
                        var newLatch = Instantiate(LatchPrefab);
                        newLatch.transform.SetParent(Controls.transform);

                        newLatch.transform.localScale = new Vector3(
                            newLatch.transform.localScale.x * Extrudable.gameObject.transform.localScale.x,
                            newLatch.transform.localScale.y * Extrudable.gameObject.transform.localScale.y,
                            newLatch.transform.localScale.z * Extrudable.gameObject.transform.localScale.z
                        );

                        var edgeCenter = edgeCenters[j];
                        edgeCenter.x *= Extrudable.gameObject.transform.localScale.x;
                        edgeCenter.y *= Extrudable.gameObject.transform.localScale.y;
                        edgeCenter.z *= Extrudable.gameObject.transform.localScale.z;

                        newLatch.transform.position =
                            Extrudable.gameObject.transform.position + edgeCenter; // + center) / 2;

                        var adjacentFaceNormal = manifold.GetFaceNormal(adjacentFace);
                        var normal = manifold.GetFaceNormal(i);
                        Quaternion latchRotation = Quaternion.LookRotation(-(adjacentFaceNormal + normal));
                        newLatch.transform.rotation = latchRotation;

                        var latchController = newLatch.GetComponent<LatchController>();
                        latchController.FirstFace = i;
                        latchController.SecondFace = adjacentFace;

//                    latchController.Locked = closedLatches.Find(facePair => latchController.IsAdjacent(facePair.Key, facePair.Value));
                        foreach (var closedLatch in closedLatches)
                        {
                            if (latchController.IsAdjacent(closedLatch.Key, closedLatch.Value))
                            {
                                latchController.Locked = true;
                                break;
                            }
                        }

//                        if (latchController.Locked)
//                            Debug.Log("Creating latch: " + latchController.FirstFace + "-" +
//                                      latchController.SecondFace + " and it is locked : " + latchController.Locked);
                        latchController.UpdateMaterialColor();

                        facesVisited[i] = 1;

                        currentHandle.GetComponent<FaceHandleController>().AttachLatch(newLatch);
                        _latches.Add(newLatch);
                    }
                }
            }

//            Debug.Log("===== NOW THERE ARE: " + _latches.Count + " NEW LATCHES ====");
//            foreach (var latch in _latches)
//            {
//                var controller = latch.GetComponent<LatchController>();
//                if (controller.Locked)
//                    Debug.Log("Latch: " + controller.FirstFace + "-" + controller.SecondFace + " is locked: " +
//                              controller.Locked);
//            }
//
//            Debug.Log("============================================================");

            UpdateVertexHandles(manifold);
        }
    }
}