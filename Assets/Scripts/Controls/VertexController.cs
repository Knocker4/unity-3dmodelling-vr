﻿using UnityEngine;

namespace Controls
{
    public class VertexController : IInteractableObject
    {
        public bool IsDragged = false;
        public int AssociatedVertexID;

        public ExtrudableMesh Extrudable;

        private FixedJoint _joint;

        // Update is called once per frame
        void Update()
        {
            if (_joint != null)
                Extrudable.MoveVertexTo(
                    AssociatedVertexID,
                    transform.position);
        }

        public override void Interact()
        {
            IsDragged = true;
            _joint = gameObject.AddComponent<FixedJoint>();
            _controllerObject.GetComponent<OvrAvatarHand>().AttachBody(_joint);
        }

        public override void ChangeInteraction(InteractionMode mode = InteractionMode.SINGLE)
        {
        }

        public override void StopInteraction()
        {
            if (_joint != null)
            {
                IsDragged = false;
                Extrudable.MoveVertexTo(
                    AssociatedVertexID,
                    transform.position);
            }
        }
    }
}