﻿using System.Collections;
using System.Collections.Generic;
using Assets.GEL;
using UnityEngine;

public class ExtrudableBackstackManager
{

	private Stack<Manifold> _manifoldStack;

	public ExtrudableBackstackManager()
	{
		_manifoldStack = new Stack<Manifold>();
	}

	public void SaveState(Manifold manifold)
	{
		_manifoldStack.Push(manifold.Copy());
	}

	public Manifold RestoreState()
	{
		return _manifoldStack.Count > 1 ? _manifoldStack.Pop() : _manifoldStack.Peek();
	}
	
}
