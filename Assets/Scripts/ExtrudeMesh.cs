﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtrudeMesh : MonoBehaviour
{
    private Mesh _srcMesh;
    private MeshExtrusion.Edge[] _edges;
    public Boolean IsExtruding;
	public int segments = 1;
	public Boolean invertFaces = false;

	// Use this for initialization
	void Start ()
	{
	    Time.timeScale = 0.1f;
	    _srcMesh = GetComponent<MeshFilter>().sharedMesh;

	    _edges = MeshExtrusion.BuildManifoldEdges(_srcMesh);
    }
	
	private   class ExtrudedSection
	{
		public Vector3 point;
		public Matrix4x4 matrix;
	}
	
    public void ExtrudeTo(Vector3 destination)
    {
//        Debug.Log("Starting extrusion");
//
	    _srcMesh = GetComponent<MeshFilter>().sharedMesh;
//
	    _edges = MeshExtrusion.BuildManifoldEdges(_srcMesh);
//	    
	    
	    Debug.Log(_srcMesh.vertices);
	    var i = 0;
	    foreach (Vector3 vertex in _srcMesh.vertices)
	    {
		    Debug.Log(i + " " + vertex);
		    i += 1;
	    }

	    i = 0;
	    foreach (int triangle in _srcMesh.triangles)
	    {
		    Debug.Log(i + " " + triangle);
		    i += 1;
	    }
	    
	    MeshExtrusion.Edge[] edgesToExtrude = {_edges[0]};
	    
	    
	    
        var sections = new Matrix4x4[2];
        sections[0] = transform.worldToLocalMatrix * Matrix4x4.TRS(transform.position, Quaternion.identity, Vector3.one);
        sections[1] = transform.worldToLocalMatrix * Matrix4x4.TRS(destination, Quaternion.identity, Vector3.one);
//		
//	    
        Debug.Log("Just before extruding");
//        MeshExtrusion.ExtrudeMesh(_srcMesh, (GetComponent<MeshFilter>()).mesh, sections, edgesToExtrude, false);
//	    	    
//	    Vector3 extrusionNormal = (destination.normalized - transform.position.normalized).normalized;
// 
//	    List<ExtrudedSection> sections = new List<ExtrudedSection>();
//
//
//	    ExtrudedSection section = new ExtrudedSection
//	    {
//		    point = transform.position,
//		    matrix = transform.localToWorldMatrix
//	    };
//	    sections.Insert(0,section);
// 
//	    for(int i = 0; i<segments; i++)
//	    {
//		    transform.position = transform.position + extrusionNormal * 1.0f / segments;
//		    section = new ExtrudedSection
//		    {
//			    point = transform.position,
//			    matrix = transform.localToWorldMatrix
//		    };
////		    sections.Insert(0, section);
//		    sections.Add(section);
//	    }
// 
//	    Matrix4x4 worldToLocal = transform.worldToLocalMatrix;
//	    Quaternion rotation = Quaternion.LookRotation(-extrusionNormal, Vector3.up);
//	    Matrix4x4[] finalSections = new Matrix4x4[sections.Count];
//	    for (int i = 0; i < sections.Count; i++)
//	    {
//		    finalSections[i] = worldToLocal * Matrix4x4.TRS(sections[i].point, rotation, Vector3.one);
//	    }
// 
//	    MeshExtrusion.ExtrudeMesh(_srcMesh, GetComponent<MeshFilter>().mesh, finalSections, _edges, invertFaces);

    }
	
	public void Extrude()
	{
		Vector3 extrusionNormal = transform.forward;
 
		List<ExtrudedSection> sections = new List<ExtrudedSection>();


		ExtrudedSection section = new ExtrudedSection
		{
			point = transform.position,
			matrix = transform.localToWorldMatrix
		};
		sections.Insert(0,section);
 
		for(int i = 0; i<segments; i++)
		{
			transform.position = transform.position + extrusionNormal * 1.0f / segments;
			section = new ExtrudedSection
			{
				point = transform.position,
				matrix = transform.localToWorldMatrix
			};
			sections.Insert(0, section);
		}
 
		Matrix4x4 worldToLocal = transform.worldToLocalMatrix;
		Quaternion rotation = Quaternion.LookRotation(-extrusionNormal, Vector3.up);
		Matrix4x4[] finalSections = new Matrix4x4[sections.Count];
		for (int i = 0; i < sections.Count; i++)
		{
			finalSections[i] = worldToLocal * Matrix4x4.TRS(sections[i].point, rotation, Vector3.one);
		}
 
		MeshExtrusion.ExtrudeMesh(_srcMesh, GetComponent<MeshFilter>().mesh, finalSections, _edges, invertFaces);
 
	}
}
