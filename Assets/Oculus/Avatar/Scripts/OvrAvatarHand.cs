﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Controls;

public class OvrAvatarHand : MonoBehaviour
{
    public enum State
    {
        EMPTY,
        TOUCHING,
        HOLDING
    };

    private const String INTERACTABLE_LAYER = "InteractableObject";

    public OVRInput.Controller Controller = OVRInput.Controller.RTouch;
    public State HandState = State.EMPTY;
    public bool IgnoreContactPoint = false;

    public OvrAvatarHand OtherHand;

    private Rigidbody _attachPoint = null;
    private FixedJoint _tempJoint;
    [SerializeField] private IInteractableObject _interactableObject;
    [SerializeField] private Boolean _shouldForgetObject = false;


    void Start()
    {
        if (_attachPoint == null)
        {
            _attachPoint = GetComponent<Rigidbody>();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
//            Debug.Log("Entered collider of: " + collider.gameObject.name);

        _shouldForgetObject = false;
        if (HandState == State.EMPTY || HandState == State.TOUCHING)
        {
            var temp = collider.gameObject;
            if (temp != null && temp.layer == LayerMask.NameToLayer(INTERACTABLE_LAYER) &&
                temp.GetComponent<IInteractableObject>() != null)
            {
                _interactableObject = temp.GetComponent<IInteractableObject>();
                HandState = State.TOUCHING;
//                    Debug.Log("Touching " + _interactableObject.tag);
            }
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer(INTERACTABLE_LAYER))
        {
            //Debug.Log("Left collider of: " + collider.gameObject.name);

            if (HandState != State.HOLDING)
            {
                _interactableObject = null;
                HandState = State.EMPTY;
            }
            else
            {
                _shouldForgetObject = true;
            }
        }
    }


    void Update()
    {
        switch (HandState)
        {
            case State.TOUCHING:
                if (_interactableObject != null &&
                    OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, Controller) >= 0.5f)
                {
//                        Debug.Log("Starting Interaction");
//                        if (OtherHand.HandState != State.HOLDING)
//                        {
                    _interactableObject.StartInteraction(gameObject);
//                        }
//                        else
//                        {
//                            OtherHand._interactableObject
//                                .ChangeInteraction(IInteractableObject.InteractionMode.DUAL);
//                        }
                    HandState = State.HOLDING;
                }

                break;
            case State.HOLDING:
                if (_interactableObject != null)
                {
                    if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, Controller) < 0.5f)
                    {
//                            if (OtherHand.HandState == State.HOLDING)
//                            {
//                                OtherHand._interactableObject.StartInteraction(gameObject);                                
//                            }
//                            else
//                            {
                        _interactableObject.StopInteraction();
                        if (_shouldForgetObject)
                            _interactableObject = null;
                        if (_tempJoint != null)
                        {
                            DestroyImmediate(_tempJoint);
                            _tempJoint = null;
                        }

                        HandState = State.TOUCHING;
//                            }
                    }
                }

                break;
        }
    }

    public void AttachBody(FixedJoint tempJoint)
    {
        _tempJoint = tempJoint;
        _tempJoint.connectedBody = _attachPoint;
    }
}